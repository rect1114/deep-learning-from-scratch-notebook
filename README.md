# [ゼロから作る Deep Learning]のノートブック

こちらはKoki Saitoh氏著作の[ゼロから作る Deep Learning]の勉強用に作ったリポジトリとなります。

[O'reilly Japanサポートリポジトリ](https://github.com/oreilly-japan/deep-learning-from-scratch/)にあるソースコードを元に自身が分かるようにメモ等を加えたJupyter Notebookです。